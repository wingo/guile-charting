clean-docs:
	rm -f $(doc).texi
	rm -f $(doc).info
	rm -f html-stamp
	rm -rf html
	rm -f $(addprefix $(doc).,aux cp cps fn fns ky log pdf pg pg toc tp tps vr vrs)
	rm -rf $(doc).html

EXTRA_DIST=$(doc).scm make-texinfo.scm make-html.scm docs.mk

regenerate-texi:
	$(top_srcdir)/env $(srcdir)/make-texinfo.scm $(srcdir)/$(doc).scm > $(doc).texi

regenerate-html:
	$(top_srcdir)/env $(srcdir)/make-html.scm $(srcdir)/$(doc).scm
	touch $@
