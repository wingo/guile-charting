;; About the package
(define *name* "Guile Charting")
(define *description* "Making graphs and charts with Guile Scheme")
(define *version* "0.2.0")
(define *updated* "19 September 2014")
(define *authors*
  '(("Andy Wingo" . "wingo@pobox.com")))

;; Copying the documentation
(define *copyright-holder* "Andy Wingo")
(define *years* '(2014))
(define *permissions*
  "Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License or (at
your option) any later version.  You should have received a copy of the GNU Lesser General Public
License along with this manual; if not, see http://www.gnu.org/licenses/.")

;; Texinfo info
(define *texinfo-basename* "guile-charting")
(define *texinfo-category* "The Algorithmic Language Scheme")
(define *extra-texinfo-menu-entries*
  '(("Concept Index")
    ("Function Index")))
(define *texinfo-epilogue*
  `((node (% (name "Concept Index")))
    (unnumbered "Concept Index")
    (printindex (% (type "cp")))
    (node (% (name "Function Index")))
    (unnumbered "Function Index")
    (printindex (% (type "fn")))))

;; HTML foo
(define *html-relative-root-path* "../../../")

;; The modules to document
(define *modules*
  '(((charting)
     "Main charting interface")
    ((charting draw)
     "Lower-level drawing operators")))

(define *module-sources* '())
